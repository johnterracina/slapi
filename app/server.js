var app = require('express')();

var ast = getAST();
var inputs = ast.inputs;

app.get('/', function(req, res){
	res.send(200, 4);
});

for (var endpoint of ast.endpoints){
	createEndpoint(endpoint);
}

app.listen(3000);


function getAST(){
	return {
		inputs: {'x':{
			type: 'constant',
			value: 5
			}},
		endpoints: [{
			httpVerb: 'GET',
			path: '/example',
			returnValue: 'x'
		}]
	}
}

function createEndpoint(endpoint){
	if (endpoint.httpVerb === 'GET'){
		app.get(endpoint.path, function(req, res){
			var returnValue = resolveValue(endpoint.returnValue);
			res.send(200, returnValue);
		});
		console.log('created get endpoint at ' + endpoint.path)
	}
 
}

function resolveValue(value){
	// returns value or lazy loads it from expression
	var resolvedValue = inputs[value];
	if (resolvedValue.type === 'constant'){
		return resolvedValue.value;
	}
	return resolvedValue.value;
}